/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* librescl
 *
 * Copyright (C) 2013. 2014 Daniel Espinosa <esodan@gmail.com>
 *
 * librescl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * librescl is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Lscl;
using GXml;

public static int main (string[] args)
{
	GLib.Test.init (ref args);
  Test.add_func ("/librescl/save/set-file",
  () => {
    try {
      var d = new SclDocument ();
      message (d.write_string ());
      d.scl.set_instance_property ("communication");
      message (d.write_string ());
      d.scl.set_instance_property ("header");
      message (d.write_string ());
      assert (d.scl.ieds != null);
      var ied = d.scl.ieds.create_item () as tIED;
      ied.name = "TEMPLATE";
      assert (ied.access_points != null);
      var ap = ied.access_points.create_item () as tAccessPoint;
      ap.name = "AP";
      ied.access_points.append (ap);
      d.scl.ieds.append (ied);
      message (d.write_string ());
      var f = File.new_for_path (LsclTest.TEST_SAVE_DIR + "/saved1.cid");
      d.write_file (f);
      assert (f.query_exists ());
      var dscl2 = new SclDocument ();
      dscl2.read_from_file (f);
      assert (dscl2.scl.communication != null);
      assert (dscl2.scl.header != null);
      assert (dscl2.scl.ieds != null);
      assert (dscl2.scl.ieds.length == 1);
      var i = dscl2.scl.ieds.item ("TEMPLATE") as tIED;
      assert (i != null);
      assert (i.name == "TEMPLATE");
      assert (i.access_points != null);
      assert (i.access_points.length == 1);
      var a = i.access_points.item ("AP");
      assert (a != null);
      f.delete ();
    } catch (GLib.Error e) {
      stdout.printf (@"ERROR: $(e.message)");
      assert_not_reached ();
    }
  });
	return GLib.Test.run ();
}
