/*
 *
 *  LibreSCL
 *
 *  Authors:
 *
 *       Daniel Espinosa <esodan@gmail.com>
 *       PowerMedia Consulting <pwmediaconsulting@gmail.com>
 *
 *
 *  Copyright (c) 2013, 2014, 2017 Daniel Espinosa
 *  Copyright (c) 2014 PowerMedia Consulting
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GXml;
public class Lscl.tDataTypeTemplates : Serializable
{
    public tLNodeType.HashMap logical_node_types { get; set; }
    public tDOType.HashMap data_object_types { get; set; }
    public tDAType.HashMap data_attribute_types { get; set; }
    public tEnumType.HashMap enum_types { get; set; }
    construct {
      set_instance_property ("logical-node-types");
      set_instance_property ("data-object-types");
      set_instance_property ("data-attribute-types");
      set_instance_property ("enum-types");
      parse_children = false;
    }
    /**
     * Create Logical Node Type
     */
    public tLNodeType create_logical_node_type () {
        return (tLNodeType) logical_node_types.create_item ();
    }
    /**
     * Create Data Object Type
     */
    public tDOType create_data_object_type () {
        return (tDOType) data_object_types.create_item ();
    }
    /**
     * Create Data Attribute Type
     */
    public tDAType create_data_attribute_type () {
        return (tDAType) data_attribute_types.create_item ();
    }
    /**
     * Create Enum Type
     */
    public tEnumType create_enum_type () {
        return (tEnumType) enum_types.create_item ();
    }
}
