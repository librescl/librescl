/*
 *
 *  LibreSCL
 *
 *  Authors:
 *
 *       Daniel Espinosa <esodan@gmail.com>
 *       PowerMedia Consulting <pwmediaconsulting@gmail.com>
 *
 *
 *  Copyright (c) 2013-2019 Daniel Espinosa
 *  Copyright (c) 2014 PowerMedia Consulting
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GXml;
namespace Lscl
{
  public class tDOType : tIDNaming, MappeableElement
  {
    public tSDO.HashMap sdos { get; set; }
    public tDA.HashMap das { get; set; }
    [Description(nick="::iedType")]
    public string ied_type { get; set; }
    [Description(nick="::cdc")]
    public tCDCEnum cdc { get; set; }
    public string get_map_key () { return id; }

    construct  {
      set_instance_property ("sdos");
      set_instance_property ("das");
    }
    /**
    * Creates a new Status Data Object
    */
    public tSDO create_sdo () throws GLib.Error
    {
        return (tSDO) sdos.create_item ();
    }
    /**
    * Creates a Data Attribute
    */
    public tDA create_da () throws GLib.Error
    {
        return (tDA) das.create_item ();
    }
    public class HashMap : GXml.HashMap
	 {
      construct {
        try { initialize (typeof (tDOType)); }
        catch (GLib.Error e) { warning ("Error: "+e.message); }
      }
	 }
  }
}

