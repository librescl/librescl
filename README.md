# librescl
 
LibreSCL is a library to read and create IEC 61850-6 compliant XML files using SCL 

# Relocation and ownership changes

LibreSCL has been moved to continue its developing at [gitlab.com/librescl/librescl](https://gitlab.com/librescl/librescl.git), please
update your URL.

# New Maintainer and License Owner

All source code has a new owner and unrevocable license transmission to Daniel Espinosa Ortiz, so any re-licensing and other
related issues will be managed by hime in above repository.
