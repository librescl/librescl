/*
 *
 *  LibreSCL
 *
 *  Authors:
 *
 *       Daniel Espinosa <esodan@gmail.com>
 *       PowerMedia Consulting <pwmediaconsulting@gmail.com>
 *
 *
 *  Copyright (c) 2013-2017 Daniel Espinosa
 *  Copyright (c) 2014 PowerMedia Consulting
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GXml;

public class Lscl.tConnectedAP : tUnNaming, MappeableElementPairKey
{
    [Description(nick="::Address",blurb="Network address communication parameters")]
    public tAddress address { get; set; }
    [Description(nick="::GSE",blurb="Generic Station Event control blocks")]
    public tGSE.DualKeyMap gses { get; set; }
    [Description(nick="::SMV",blurb="Sample Value control blocks")]
    public tSMV.DualKeyMap smvs { get; set; }
    [Description(nick="::PhysConn",blurb="Physical Connection")]
    public tPhysConn phys_conn { get; set; }
    [Description(nick="::iedName",blurb="IED's name, connected to network")]
    public string ied_name { get; set; }
    [Description(nick="::apName",blurb="IED's Access Point's name, connected to network")]
    public string ap_name  { get; set; }
    // MappeableElementPairKey
    public string get_map_primary_key () { return ied_name; }
    public string get_map_secondary_key () { return ap_name; }

    construct  {
    set_instance_property ("gses");
    set_instance_property ("smvs");
    }
    /**
    * Create Sample Measured Value control block
    */
    public tSMV create_smv () {
        return (tSMV) smvs.create_item ();
    }
    /**
    * Create Generic Station Event control block
    */
    public tGSE create_gse () {
        return (tGSE) gses.create_item ();
    }
    /**
    * Create Physical Connection for this access point
    */
    public void create_phys_conn () {
        set_instance_property ("phys-conn");
    }

    /**
     * A hash collection with two keys, one is the
     * IED's name as the primary key and the secondary key as the
     * access point's name of the IED owner of the access point.
     */
    public class DualKeyMap : GXml.HashPairedMap {
        construct {
            try { initialize (typeof (tConnectedAP)); }
            catch (GLib.Error e) { warning ("Error: "+e.message); }
        }
    }
}

